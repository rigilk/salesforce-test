import React from 'react';

const Header = props => {
  return (
    <div>
      <div aria-label="Primary Site Navigation" role="navigation">
        <h1>
          <a href="/" data-focus-listener="true">
            <img
              alt="Brand Central Home Page"
              src="https://beam3-salesforce.s3.amazonaws.com/global/sf-logo%402x.png"
              height="32"
              className="brand-image"
            />
          </a>
          Salesforce Technical Test
        </h1>
      </div>
    </div>
  );
};

export default Header;
