import './App.scss';
import { useState } from 'react';
import Header from './components/header';
import Button from "./components/button";

function App() {
  const [ showModal, setShowModal ] = useState(false);
  console.log("showModal", showModal);

  return (
    <div className="App">
      <Header />
      <hr />
      <div className="container-action">
        <Button onClick={() => setShowModal(true)}>Open Modal!</Button>
      </div>
    </div>
  );
}

export default App;
